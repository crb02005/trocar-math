const greatestCommonDivisor = (m,n)=>{
    const r=m%n;
    return r==0?n:greatestCommonDivisor(n,r);
}

exports.greatestCommonDivisor = greatestCommonDivisor;